package stream;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		List <MonitoredData> ActivityList = null;
		
		ActivityList=read(ActivityList); //2 p
		
		display(ActivityList);	
		
		count(ActivityList);
		
		sort(ActivityList);
		
		activitiesPerDay(ActivityList);
		
	}
	
	private static List <MonitoredData> read (List<MonitoredData> mdArray)
	{
		//List <MonitoredData> mdArray;
		String fileName = "C:\\Users\\Marius\\eclipse-workspace\\stream\\Activities.txt";

		//read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

			
			//stream.forEach(System.out::println);
			mdArray=stream.map(string->string.split("		",3))
					.map(array->new MonitoredData (array[0],array[1],array[2]))
					.collect(Collectors.toList());

			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return mdArray;
		
	}
	
	private static void display (List <MonitoredData> mdArray)
	{
		for (int i=0;i<mdArray.size();i++)
		{
			MonitoredData m = new MonitoredData();
			m=mdArray.get(i);
			System.out.println(m.getStartTime() + "       " + m.getEndTime() + "       " + m.getActivity());
		}
	}
	
	private static void count (List <MonitoredData> mdArray)
	{
		List<String> stTime = 
			    mdArray.stream()
			              .map(MonitoredData::getStartTime)
			              .map(string->string.split(" ",2))
			              .map(array->array[0])
			              .collect(Collectors.toList());
		
		/*for (int i=0;i<stTime.size();i++)
		{
			System.out.println(stTime.get(i));
		}*/
		
		long l = stTime.stream().distinct().count();
		
		System.out.println("Numar de zile: " + l);
	}
	
	private static void sort (List <MonitoredData> mdArray) throws IOException 
	{
		String outputPath = "C:\\Users\\Marius\\eclipse-workspace\\stream\\Output.txt";
		List<String> activity = 
			    mdArray.stream()
			              .map(MonitoredData::getActivity)
			              .collect(Collectors.toList());
		
		
		Map<String, Long> result =
                activity.stream().collect(
                        Collectors.groupingBy(
                                Function.identity(), Collectors.counting()
                        )
                );
		
		PrintWriter outputStream = new PrintWriter (outputPath);
		outputStream.println("Activity/Appearances\n");
		result.forEach((key,value)-> outputStream.println(key + " = " + value));
		outputStream.close();
        //System.out.println(result);
        
	}
	
	private static void activitiesPerDay (List <MonitoredData> mdArray)
	{
		
		/*List <MonitoredData> days =null;
		
		days=mdArray.stream()
				.map(x->x.getStartTime())
	            .map(x->x.split(" ",2))
	            .peek(d->d.setStartTime(x))
				.map(d-> new MonitoredData (d.getStartTime(),d.getActivity()))
				.collect(Collectors.toList());
		
		for (int i=0;i<days.size();i++)
		{
			MonitoredData m = new MonitoredData();
			m=days.get(i);
			System.out.println(m.getStartTime() + "       " + m.getEndTime() + "       " + m.getActivity());
		}*/
		
	}

}
